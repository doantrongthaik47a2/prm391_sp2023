package com.fpt.demochatapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    private static int FLASH_TIMER = 3000;
    private ImageView imvLogo;
    private TextView textView;

    Animation slideLogo, slideAppName;
    public void bindingView(){
        imvLogo = findViewById(R.id.imv_logo);
        textView = findViewById(R.id.textView);
        slideLogo = AnimationUtils.loadAnimation(this,R.anim.slide_anim);
        slideAppName = AnimationUtils.loadAnimation(this,R.anim.bottom_anim);

        imvLogo.setAnimation(slideLogo);
        textView.setAnimation(slideAppName);



    }
    public void bindingAction(){
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            startActivity(intent);
            finish();
        },FLASH_TIMER );
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        bindingView();
        bindingAction();
    }
}