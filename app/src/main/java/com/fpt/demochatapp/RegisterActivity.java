package com.fpt.demochatapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private EditText username;
    private EditText email;
    private EditText password;
    private Button btn_register;
    private DatabaseReference databaseReference ;
    FirebaseAuth auth ;




    private void bindingView(){
        username =findViewById(R.id.edt_username);
        email = findViewById(R.id.edt_email);
        password = findViewById(R.id.edt_password);
        btn_register = findViewById(R.id.btn_register);
    }
    private void bindingAction(){
        btn_register.setOnClickListener(this::onRegisterOnClick);
    }

    private void onRegisterOnClick(View view) {
        String txt_username = username.getText().toString();
        String txt_email = email.getText().toString();
        String txt_password = password.getText().toString();


        if (TextUtils.isEmpty(txt_username) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
            Toast.makeText(this, "All fields are required", Toast.LENGTH_SHORT).show();
        }else if ( txt_password.length() < 6){
            Toast.makeText(this, "Password must be at least 6 character", Toast.LENGTH_SHORT).show();
        }else {
            register(txt_username, txt_email, txt_password);
            Toast.makeText(this, "Register Successfully", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        bindingView();
        bindingAction();
    }
    private  void register(final String username,String email, String password){
        auth = FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        FirebaseUser user = auth.getCurrentUser();
                        String userID = user.getUid();

                        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userID);

                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("id", userID);
                        hashMap.put("user", username);
                        hashMap.put("image", "default");

                        databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });
                    }else{
                        Toast.makeText(RegisterActivity.this, "You can't register with this email anđ password", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}