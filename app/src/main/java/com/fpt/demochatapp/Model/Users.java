package com.fpt.demochatapp.Model;

public class Users {
    private String id;
    private String image;
    private String user;

    public Users() {
    }

    public Users(String id, String image, String user) {
        this.id = id;
        this.image = image;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
