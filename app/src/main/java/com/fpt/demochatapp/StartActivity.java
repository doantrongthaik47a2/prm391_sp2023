package com.fpt.demochatapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity {
    private Button btn_login , btn_register;
    FirebaseUser firebaseUser;



    private void bindingView(){
        btn_login = findViewById(R.id.login);
        btn_register = findViewById(R.id.register);
    }
    private void bindingAction(){
        btn_register.setOnClickListener(this::RegisterOnclick);
        btn_login.setOnClickListener(this::LoginOnclick);

    }

    private void LoginOnclick(View view) {
        startActivity(new Intent(StartActivity.this, LoginActivity.class));
    }

    private void RegisterOnclick(View view) {
        startActivity(new Intent(StartActivity.this, RegisterActivity.class));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        bindingView();
        bindingAction();
    }
}